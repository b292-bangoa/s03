--Additional Activity

--Delete reviews table
drop table reviews;

--Create reviews table
create table reviews (
	id int not null auto_increment,
	review varchar(500) not null,
	datetime_created datetime not null,
	rating int not null,
	primary key (id)
);

--Create records
insert into reviews(review, datetime_created, rating) values ("The songs are okay. Worth the subscription", "2023-05-03 00:00:00", 5);
insert into reviews(review, datetime_created, rating) values ("The songs are meh. I want BLACKPINK", "2023-05-03 00:00:00", 1);
insert into reviews(review, datetime_created, rating) values ("Add Bruno Mars and Lady Gaga", "2023-03-23 00:00:00", 4);
insert into reviews(review, datetime_created, rating) values ("I want to listen to more k-pop", "2022-09-23 00:00:00", 3);
insert into reviews(review, datetime_created, rating) values ("Kindly add more OPM", "2023-02-01 00:00:00", 5);

select * from reviews;
select * from reviews where rating = 5;
select * from reviews where rating = 1;
update reviews set rating = 5;