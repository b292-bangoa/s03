--Users records:

insert into users(email, password, datetime_created) values ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
insert into users(email, password, datetime_created) values ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
insert into users(email, password, datetime_created) values ("janesmith@gmail.com", "passwordc", "2021-01-01 03:00:00");
insert into users(email, password, datetime_created) values ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
insert into users(email, password, datetime_created) values ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

--POSTS records;
insert into posts(author_id, title, content, datetime_posted) values (1, "First Code", "Hello World!", "2021-01-01 01:00:00");
insert into posts(author_id, title, content, datetime_posted) values (1, "Second Code", "Hello Earth!", "2021-01-01 02:00:00");
insert into posts(author_id, title, content, datetime_posted) values (2, "Third Code", "Welcome to Mars!", "2021-01-01 03:00:00");
insert into posts(author_id, title, content, datetime_posted) values (4, "Fourth Code", "Bye bye solar system!", "2021-01-01 04:00:00");

--Get all post from author_id 1:
select * from posts where author_id = 1;
--Get all users email and datetime_creation;
select email, datetime_created from users;
--Update post's content from "Hello Ea.." to "Hello to the .."
update posts set content = "Hello to the people of the Earth" where id = 2; 
--Delete user with email "john.."
delete from users where email = "johndoe@gmail.com";