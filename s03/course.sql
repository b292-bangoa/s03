create database course_db;

drop database course_db;

create database course_db;

use course_db;

create table students(
	id INT NOT NULL auto_increment,
	username varchar(50) not null,
	password varchar(50) not null,
	full_name varchar(50) not null,
	email varchar(50),
	PRIMARY KEY (id)
);

create table subjects(
	id INT NOT NULL auto_increment,
	course_name varchar(100) not null,
	schedule varchar(100) not null,
	instructor varchar(100) not null,
	PRIMARY KEY (id)
);

create table enrollments(
	id INT NOT NULL auto_increment,
	student_id int not null,
	subject_id int not null,
	datetime_created timestamp default current_timestamp,
	PRIMARY KEY (id),
	constraint fk_student_id
		foreign key (student_id) references students(id)
		on update cascade
		on delete restrict,
	constraint fk_subject_id
		foreign key (subject_id) references subjects(id)
		on update cascade
		on delete restrict
);

--Add examples:
insert into students(username, password, full_name, email) values ("aaa", "pw1", "A aa", "a@mail.co");
insert into subjects(course_name, schedule, instructor) values ("SQL", "5-10PM", "Ms Camille");
insert into enrollments(student_id, subject_id) values (1,1);

